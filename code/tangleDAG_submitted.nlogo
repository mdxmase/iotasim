extensions [nw]

breed [nodes node] ; each node on the graph is a transaction

breed [walkers walker] ; used for tip selection when the MCMC algorithm is switched on

directed-link-breed [edges edge]


globals [
;  genesis ; self-explanatory: creates 1 or more tips at the beginning
;  lambda  ; the average number of new tips randomly created (per tick) in the simulation
;  alpha
  colour     ; used for colouring the tangle according to history
  counter    ; used to count walker's position
; nw  ;;;;;;;;;;; the number of walkers we want to use for the MCMC tip selection strategy; say, 10 to 10000, can use a slider in netlogo instead of globals.
]

nodes-own [  ;
; ow  ;;;;;;;;;;;  own weight: we assume that each node has own-weight 1, for simplicity, but could be a different one
cw    ;;;;;;;;;;;  cumulative weight: the sum of own weights of all (future) transactions that directly or indirectly approve the transaction plus the own weight of the transaction itself. For tips, cw = 1 or ow.
; sc  ;;;;;;;;;;; score: the sum of own weights of all transactions approved by this transaction plus  the own weight of the transaction itself.
; ht  ;;;;;;;;;;; height: the length of the longest oriented path to the genesis transaction
; dp  ;;;;;;;;;;; depth: the length of the longest reverse-oriented path to some tip
]

;walkers-own [ pos ] ; the position of the random walkers  --- uncommented when used.

to setup-tangle

  clear-all

  ask nodes [ set color red ]
  ask edges [ set color gray ]
  set-default-shape nodes "circle"
  create-nodes genesis  ;;;;;;;;;;; the "genesis" transaction: creates a number of initial tips

  ask node 0 [
    create-edges-from other nodes
    set cw 2 ]

  ask other nodes [ set cw 1 ]    ; it's the genesis transaction so by definition its offspring are the initial tips.
  layout-radial nodes edges node 0     ;;;;;;;;;;; radially from genesis
  reset-ticks

end

to grow-tangle

  ;set-default-shape nodes "circle"
  approve-nodes ;;;;;;;;;;;   find-tips  ---- the tip finding algorithm  ; care should be taken with parameters when initialising to avoid node mismatch
  update-cw     ;;;;;;;;;;;   the nodes dynamically update their weights.

 layout-spring nodes edges 0.5 2 1 ; 0.2 5 0.5   ;;;;;;;;;;; spring-loaded display (slightly slower than radial, but not by much)
 ;layout-radial nodes edges node 0                ;;;;;;;;;;; alternative visualisation, too

 ; nw:save-matrix "~/adjacency.txt"              ;;;;;;;;;;; saved for feedback if we want to recalculate on the fly during MCMC algo while the tangle grows
 ; nw:save-graphml "~/tanglegraph.graphml"    ;;;;;;;;;;; saves the current graph: better used in observer the command window once the growth of the tangle has been stopped.

  ;;;;;;;;;;;   creates the "colour history of the tangle", commenting out the two lines below leaves the nodes with the desired intial colour, set as red above

   ask nodes with [cw = 1] [set color colour]
   set colour colour + 0.5 ; 1 0-139 fractional scale from the discrete color swatches in netlogo (use a high-contrast version)

  tick   ;;;;;;;;;;;   next discrete step

end

to-report incoming-tips            ;;;;;;;;;;; we create a poissonian clock of new tips
  report random-poisson lambda     ;;;;;;;;;;; lambda is, on average, the number of incoming tips per unit time
                                   ;;;;;;;;;;; TODO: implement total tips = hidden + revealed tips under different assumptions, possibly combining two poisson clocks.
end

to-report old-nodes ; stub
  report count nodes
end

to-report find-tips
  report [one-of old-nodes with [cw = 1] ] of nodes
end

to approve-nodes ; [oldnodes]

  ;;;;;;;;;;;  DAG grown UNIFORMLY AT RANDOM as in initial assumption of the tangle paper. Conceptually. very simple to define
  ;;;;;;;;;;;  the algorithm can be made more complicated if we assume nodes have other characteristics in addition to cw
  ;;;;;;;;;;;  It is very fast and tractable even with the JVM+Netlogo+Jung overhead, in part because the adjacency matrix has an extremely simple form).

  create-nodes incoming-tips [ set color red
                               create-edges-to n-of 2 other nodes with [cw = 1] ;; there should always be more than 2 available tips, otherwise the random choice fails and an error message with NOBODY appears.
                               ]  ;; If genesis and lambda are set at sensible values, this usually doesn't happen.

  ;;;;;;;;;;; Implementations of other tip selection algorithms such as MCMC also appear here.
  ;;;;;;;;;;; Warning: while it is visually indistinguishable from the above, the strict implementation considerably slows down the simulation of the tangle after a while. Workaround with nw:path-to has a bug.

end

to update-cw
  ask nodes [ set cw cw + 1 ]
end

to-report number-of-tips
  report count nodes with [ cw = 1 ]
end


;;;;;; TODO: do the calculations for depths and scores when needed

;;;;;;;;;;;  if we want to do a separate layout function

;to layout
;  layout-spring nodes links 0.5 2 1
;end


to rw-tips ; rw random walk towards two tips in the current graph. In final form it should be a reporter, not a procedure, giving two nodes with which to attach. Warning: code is still buggy.  |||||||||||| TIP SELECTION ALGORITHM with weight CW unused

  ;;;;;;;;;;;   A simpler way to implement the MCMC algo is for every tick, to put nw walkers at node 0, then walk the reverse-oriented paths with probability 1/d_out at each node (or the max incoming degree) in the following way: at each intermediate step
  ;;;;;;;;;;;   where the walker is in node j (who) , move is to one-of edge-neighbors with [cw(j) = cw(j) - 1], can also use nested ifs and compare with using random-float 1
  ;;;;;;;;;;;   then we stop the walk when cw [j] = 1, and record its "who" number, then add it to a list, which is sorted in terms of frequency.
  ;;;;;;;;;;;   When it has converged, be it with 10, 100, 10000 walkers, this means that the walkers agree on the two most probable lists where to attach.

  ;;;;;;;;;;;   walk feedback code below, computationally expensive

  ; put some "walker" turtles on the network. The button rw on the interface basically gives a visual feedback of the walk.

    ;;;; note: under review for a bug in version 0.1, new version released in 0.3 ;;;;;;;;

    ;;;;;;;;;;; until two distinct tips are found, stick to them

    ;;;;;;;;;;; calculate dynamically the transition probabilities via the adjacency matrix

    ;;;;;;;;;;; update transitions

    ;tick
end

;to markov-transition
  ;  let alpha 0.5  better declared in globals/ slider
  ;  let trans-prob exp ( - alpha ( cw of starting-node - cw of destination-node )  /  sum exp ( - alpha ( cw of starting-node - cw of destination-node ))
  ;
;end

;to graph-edges
 ; set-current-plot "edge-distribution"
  ;set-plot-x-range 1  1 + max [count link-neighbors] of turtles
  ;histogram  [count edge-neighbors] of nodes
;end
@#$#@#$#@
GRAPHICS-WINDOW
191
0
5249
5059
-1
-1
50.0
1
10
1
1
1
0
0
0
1
-50
50
-50
50
1
1
1
ticks
30.0

BUTTON
27
133
160
166
NIL
setup-tangle
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
11
10
183
43
genesis
genesis
2
100
10.0
1
1
NIL
HORIZONTAL

SLIDER
11
44
183
77
lambda
lambda
1
100
15.0
1
1
NIL
HORIZONTAL

BUTTON
25
173
159
206
NIL
grow-tangle
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
23
212
160
245
random walk to tips
rw-tips\n\n\n
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
-2
313
191
458
number of tips random
ticks
not
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -7858858 true "" "plot count nodes with [ cw = 1 ]"

MONITOR
34
1042
145
1087
NIL
number-of-tips
17
1
11

PLOT
-4
456
190
607
cumulative weight
ticks
cw
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "ask turtle 0 [plot cw]"

SLIDER
11
81
183
114
alpha
alpha
0.001
10
0.02
0.01
1
NIL
HORIZONTAL

PLOT
-9
608
191
758
edge distribution
edge-neighbors
freq
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" " histogram  [count edge-neighbors] of nodes"

SLIDER
6
799
178
832
num-walkers
num-walkers
0
100
8.0
1
1
NIL
HORIZONTAL

CHOOSER
22
253
160
298
strategy
strategy
"greedy" "breadth-first" "depth-first" "parasite"
0

@#$#@#$#@
## WHAT IS IT?

This is the main code used for the simulations of the random Tangle used for simulating the IOTA platform in the paper "Multi-agent Based Simulations of Block-free Distributed Ledgers" submitted for the IEEE E3WSN - AINA-2018 Workshop, 16-18 May 2018, Pedagogical University of Cracow, Poland

(c) Michele Bottone 2018, released under the GPL license. This NetLogo model is in active development as part of a larger DAG project. For information, please send an email to m.bottone@mdx.ac.uk

version 0.1 16/1/2018
version 0.2 16/2/2018 - bug ticket 
version 0.3 16/3/2018 

## HOW IT WORKS

The mathematical model of the Tangle DAG is simple to describe: From a genesis transaction, new nodes (representing single transactions) arrive and try to attach to (approve) two older transactions. Each node has an internal variable called "cw" that is updated. Its evolution can be complex. 

## HOW TO USE IT

The model is initialised by selecting two required "genesis" and "lambda" parameters and two optional "alpha" and "num-walkers" parameters and by clicking on the "setup-tangle" button. Genesis creates a fixed number of (genesis - 1) initial transactions which become tips; lambda creates a mean number of new transaction per discrete unit of time (tick) that try to attach to the tangle. Alpha controls the randomness of a lattice walk.

The tangle grows using random choice provided automatically by the NetLogo environment, using the forever button "grow-tangle".

An alternative growth model is the basis of the "rw-tips" button, labelled "random walk to tips" 


## THINGS TO NOTICE

Changing genesis, which is an initial parameter, is not recommended.

For better visuals, switch to 3D View in the Interface tab.

A max-pxcor and min-pxcor and patch size of at least 50 is recommended, set is as high as the system allows if your main purpose is visualisation.

Also, note that usually NetLogo ships with a one-gigabyte ceiling on how much total RAM it can use, and when it hits that ceiling it shows a "too big" error message.
So if your RAM exceeds that, and you want to run large systems, you can manually change the size of the Java Virtual Machine, as explained here:

http://ccl.northwestern.edu/netlogo/docs/faq.html#howbig

This is particularly relevant for the more complicated tip selection strategies.

## THINGS TO TRY AND EXTENDING THE MODEL

Lambda can be varied simply by changing the slider. You can do this either when the system is running in "tangle-growth" mode (it is a forever button) or when the button is paused.

You can also modify the approval procedure to make the model such that it tries to attach to a number of tips different from 2.

Finally, "cw" can be a lot more complex.

If you want to export the tangle data for use in another program, simply stop the forever button for the tangle growth and write either of the following in the Observer field:

nw:save-matrix "~/path/to/adjacency.txt"  to save the adjacency matrix of the graph
nw:save-graphml "~/path/to/tanglegraph.graphml"  to save the current tangle graph

 

## NETLOGO FEATURES

MCMC tip selection, parasite, greedy, breadth-first and depth-first speedup under review as in http://jasss.soc.surrey.ac.uk/20/1/3.html 

## RELATED MODELS

Preferential Attachment, Random Walk on a Lattice, Random Graph

## CREDITS AND REFERENCES

http://eprints.mdx.ac.uk/23578/

@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.2
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
