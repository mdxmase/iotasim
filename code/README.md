# README #

### What is this repository for? ###

This is the main code used for the simulations of the random Tangle used for simulating the IOTA platform in the paper "Multi-agent Based Simulations of Block-free Distributed Ledgers" submitted for the IEEE E3WSN - AINA-2018 Workshop, 16-18 May 2018, Pedagogical University of Cracow, Poland

(c) Michele Bottone 2018, released under the GPL license. This NetLogo model is in active development as part of a larger DAG project. For information, please send an email to m.bottone@mdx.ac.uk

* version 0.1 16/1/2018
* version 0.2 16/2/2018 - bug ticket 
* version 0.3 16/3/2018 

### HOW IT WORKS ###

The mathematical model of the Tangle DAG is simple to describe: From a genesis transaction, new nodes (representing single transactions) arrive and try to attach to (approve) two older transactions. Each node has an internal variable called "cw" that is updated. Its evolution can be complex. 

### HOW TO USE IT ###

The model is initialised by selecting two required "genesis" and "lambda" parameters and two optional "alpha" and "num-walkers" parameters and by clicking on the "setup-tangle" button. Genesis creates a fixed number of (genesis - 1) initial transactions which become tips; lambda creates a mean number of new transaction per discrete unit of time (tick) that try to attach to the tangle. Alpha controls the randomness of a lattice walk.

The tangle grows using random choice provided automatically by the NetLogo environment, using the forever button "grow-tangle".

An alternative growth model is the basis of the "rw-tips" button, labelled "random walk to tips"

### THINGS TO NOTICE ###

Changing genesis, which is an initial parameter, is not recommended.

For better visuals, switch to 3D View in the Interface tab.

A max-pxcor and min-pxcor and patch size of at least 50 is recommended, set is as high as the system allows if your main purpose is visualisation.

Also, note that usually NetLogo ships with a one-gigabyte ceiling on how much total RAM it can use, and when it hits that ceiling it shows a "too big" error message.
So if your RAM exceeds that, and you want to run large systems, you can manually change the size of the Java Virtual Machine, as explained here:

http://ccl.northwestern.edu/netlogo/docs/faq.html#howbig

This is particularly relevant for the more complicated tip selection strategies.

### THINGS TO TRY AND EXTENDING THE MODEL ###

Lambda can be varied simply by changing the slider. You can do this either when the system is running in "tangle-growth" mode (it is a forever button) or when the button is paused.

You can also modify the approval procedure to make the model such that it tries to attach to a number of tips different from 2.

Finally, "cw" can be a lot more complex.

If you want to export the tangle data for use in another program, simply stop the forever button for the tangle growth and write either of the following in the Observer field:

nw:save-matrix "~/path/to/adjacency.txt"  to save the adjacency matrix of the graph
nw:save-graphml "~/path/to/tanglegraph.graphml"  to save the current tangle graph

 

### NETLOGO FEATURES ###

MCMC tip selection, parasite, greedy, breadth-first and depth-first speedup under review as in http://jasss.soc.surrey.ac.uk/20/1/3.html 

### RELATED MODELS ###

Preferential Attachment, Random Walk on a Lattice, Random Graph

### CREDITS AND REFERENCES ###

http://eprints.mdx.ac.uk/23578/
